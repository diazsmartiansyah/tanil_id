﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class AnimasiTombol : MonoBehaviour
{
    private bool isOnFlash = false;

    public void _Animasi()
    {
        GetComponent<Animation>().Play("button");
        KumpulanSuara.instance.Panggil_Suara(0);
    }

    public void toggleFlash()
    {
        if (!isOnFlash)
        {
            CameraDevice.Instance.SetFlashTorchMode(true);
            isOnFlash = true;
        }
        else
        {
            CameraDevice.Instance.SetFlashTorchMode(false);
            isOnFlash = false;
        }
    }
}   
