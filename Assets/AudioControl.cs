﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControl : MonoBehaviour
{
    public AudioSource lagu;

    public void mainkan()
    {
        lagu.Play();
    }
    public void berhenti()
    {
        lagu.Stop();
    }
    public void delay()
    {
        lagu.Pause();
    }
}
