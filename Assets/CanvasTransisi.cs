﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasTransisi : MonoBehaviour
{
    public static string NameScene;


    public void btn_pindah(string nama)
    {
        this.gameObject.SetActive(true);
        NameScene = nama;
        GetComponent<Animator>().Play("end");
    }


    public void Object_InActive()
    {
        this.gameObject.SetActive(false);
    }

    public void Pindah_Scene()
    {
        SceneManager.LoadScene(NameScene);
    }


    public void btn_keluargame()
    {
        Application.Quit();
    }

}
